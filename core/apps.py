from django.apps import AppConfig


class SigcomCoreConfig(AppConfig):
    name = 'core'
