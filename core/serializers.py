
from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from core.models import Comissionamento, Funcao, GrandeComando
from core.models import Missao, ModuloMissao, PlanoComissionamento
from core.models import Setor, UnidadeOrganizacional, User


class UserSerializer(serializers.ModelSerializer):
    """ Serializador de usuários
    """

    class Meta:
        model = User
        extra_kwargs = {'password': {'write_only': True}}
        fields = ['id', 'name', 'email', 'cpf', 'military_rank',
                  'military_name', 'military_career',
                  'military_speciality', 'military_subspeciality',
                  'military_id', 'civilian_career',
                  'civilian_level', 'civilian_position',
                  'civilian_position_acronym', 'civilian_class_reference',
                  'identity_document_type', 'identity_document_number',
                  'identity_document_issuer', 'identity_document_uf',
                  'identity_document_nationality', 'avatar',
                  'dependente']


class GrandeComandoSerializer(serializers.ModelSerializer):
    """Nesta linha de baixo fazemos com que apareça o que foi
    definido no método toString da entidade UnidadeOrganizacional."""
    # unidades = serializers.StringRelatedField(many=True)
    """---------------------------------------------------------- """
    class Meta:
        model = GrandeComando
        fields = ['id', 'sigla', 'nome', 'ativo', 'unidades']


class UnidadeOrganizacionalSerializer(serializers.ModelSerializer):
    """ Serializador de Unidade Organizacional
    """
    class Meta:
        model = UnidadeOrganizacional
        fields = ['id', 'sigla', 'nome', 'ativo', 'grandeComando']


class AccountSerializer(serializers.ModelSerializer):
    """ Serializador do usuário logado (somente dados básicos)
    """

    class Meta:
        model = User
        fields = (
            'username', 'first_name', 'last_name', 'is_active', 'email',
            'groups', 'avatar'
        )


class SetorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Setor
        fields = '__all__'


class FuncaoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Funcao
        fields = '__all__'


class PlanoComissionamentoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlanoComissionamento
        fields = ['id', 'ano', 'unidade', 'valor_total_previsto',
                  'valor_total_executado']


class ComissionamentoSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    unidade = UnidadeOrganizacionalSerializer()
    plano = PlanoComissionamentoSerializer()
    responsavel_credito = UnidadeOrganizacionalSerializer()
    ug_responsavel = UnidadeOrganizacionalSerializer()
    ug_pagadora = UnidadeOrganizacionalSerializer()
    class Meta:
        model = Comissionamento
        fields = '__all__'


class MissaoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Missao
        fields = ['id', 'descricao', 'comissionamento', 'user',
                  'valorTotalAjudaCusto',
                  'valorTotalDiarias', 'comissionamento']


class ModuloMissaoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModuloMissao
        fields = ['id', 'inicio', 'termino', 'ordem_servico', 'unidade',
                  'descricao', 'missao', 'ano', 'localidade',
                  'valor', 'numero_dias']


class TokenObtainPairPatchedSerializer(TokenObtainPairSerializer):
    """ Adiciona o usuário na resposta do token JWT
    """

    def validate(self, attrs):
        """ Adiciona o usuário na resposta
        """
        data = super().validate(attrs)

        # Custom data you want to include
        user = UserSerializer(self.user)
        data.update({'user': user.data})

        return data
