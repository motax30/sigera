""" Módulo relacionado à conversão de datas
"""

from datetime import datetime

MONTH_NAME_MAP = {
    1: "janeiro",
    2: "fevereiro",
    3: "março",
    4: "abril",
    5: "maio",
    6: "junho",
    7: "julho",
    8: "agosto",
    9: "setembro",
    10: "outubro",
    11: "novembro",
    12: "dezembro",
}


def now():
    """ Retorna a data atual no formato "1º de dezembro de 2020"
    """
    return convert_to_pt()


def convert_to_pt(date=None):
    """ Converte uma data para o formato "1º de dezembro de 2020"
    """

    if date is None:
        date = datetime.now()

    day = str(date.day) if 2 <= date.day <= 31 else '1º'
    month = MONTH_NAME_MAP[date.month]
    year = date.year

    return "%s de %s de %s" % (day, month, year)
