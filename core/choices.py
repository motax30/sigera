""" Módulo para opções comuns aos modelos
"""

BRAZILIAN_STATES = [
    ('AC', 'Acre'),
    ('AL', 'Alagoas'),
    ('AP', 'Amapá'),
    ('AM', 'Amazonas'),
    ('BA', 'Bahia'),
    ('CE', 'Ceará'),
    ('DF', 'Distrito Federal'),
    ('ES', 'Espírito Santo'),
    ('GO', 'Goiás'),
    ('MA', 'Maranhão'),
    ('MT', 'Mato Grosso'),
    ('MS', 'Mato Grosso do Sul'),
    ('MG', 'Minas Gerais'),
    ('PA', 'Pará'),
    ('PB', 'Paraíba'),
    ('PR', 'Paraná'),
    ('PE', 'Pernambuco'),
    ('PI', 'Piauí'),
    ('RJ', 'Rio de Janeiro'),
    ('RN', 'Rio Grande do Norte'),
    ('RS', 'Rio Grande do Sul'),
    ('RO', 'Rondônia'),
    ('RR', 'Roraima'),
    ('SC', 'Santa Catarina'),
    ('SP', 'São Paulo'),
    ('SE', 'Sergipe'),
    ('TO', 'Tocantins'),
]


DOCUMENT_LIST = [
    ('ID_MILITAR', 'Carteira de Identidade Militar (RA, RE, RM)'),
    ('RG', 'Carteira de Identidade Civil (RG)'),
    ('CARTEIRA',
     'Carteira de Identificação de Orgão Fiscalizador (Ordens, Conselhos, etc)'
     ),
    ('Passaporte', 'Passaporte'),
    ('CARTEIRA_MIN_PUBLICO', 'Carteira Funcional do Ministério Público'),
    ('RESERVISTA', 'Certificado de Reservista'),
    ('CARTEIRA_FUNCIONAL)', 'Carteira Funcional Expedida por Órgão Público'),
    ('CTPS', 'Carteira de Trabalho e Previdência Social'),
    ('CNH', 'Carteira Nacional de Habilitação')
]

CIRCULOS_HIERARQUICOS = [
    ('Comandantes','Comandantes da Marinha, Exército e Aeronáutica, cargos de Natureza Especial'),
    ('Oficiais-Generais','Oficiais-Generais'),
    ('Oficiais-Superiores','Oficiais-Superiores'),
    ('Of Interm e Subalternos','Oficiais-Intermediários, Oficiais Subalternos, Guardas-Marinha e Aspirante a Oficial'),
    ('Graduados','Suboficiais, Subtenentes, Sargentos, Aspirantes e Cadetes'),
    ('Alunos','Alunos do Centro de Formação de Oficiais da Aeronáutica, de órgão de preparação de oficiais de reserva, alunos do Colégio Naval e das escolas preparatórias de cadetes'),
    ('Demais Praças','Demais Praças e Praças Especiais')
]

GRUPO_ESTADO_CIDADE = [
    ('DF/ MN/ RJ','Deslocamentos para Brasília Manaus/Rio de Janeiro'),
    ('Belo Horizonte/Fortaleza/Porto Alegre/Recife/ Salvador/São Paulo',' Deslocamentos para Belo Horizonte/Fortaleza/Porto Alegre/Recife/ Salvador/São Paulo'),
    ('Demais Estados','Deslocamentos para outras capitais de Estados'),
    ('Demais deslocamentos','Demais deslocamentos')
]