from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework_simplejwt.views import TokenObtainPairView
from .serializers import TokenObtainPairPatchedSerializer
from core.serializers import AccountSerializer, ComissionamentoSerializer
from core.serializers import FuncaoSerializer, GrandeComandoSerializer
from core.serializers import MissaoSerializer
from core.serializers import ModuloMissaoSerializer
from core.serializers import PlanoComissionamentoSerializer
from core.serializers import SetorSerializer
from core.serializers import UnidadeOrganizacionalSerializer, UserSerializer
from core.models import Comissionamento, Funcao, GrandeComando
from core.models import Missao, ModuloMissao, PlanoComissionamento, Setor
from core.models import UnidadeOrganizacional, User
from rest_framework.response import Response
from rest_framework import permissions
from djangorestframework_camel_case.util import camelize
from django.shortcuts import get_object_or_404


class TokenObtainPairPatchedView(TokenObtainPairView):
    """
    Takes a set of user credentials and returns an access and refresh JSON web
    token pair to prove the authentication of those credentials.
    """

    serializer_class = TokenObtainPairPatchedSerializer

    token_obtain_pair = TokenObtainPairView.as_view()


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [permissions.IsAuthenticated]
    filter_fields = ('id', 'username')

    # pylint: disable=unused-argument
    def retrieve(self, request, *args, **kwargs):
        """ Habilitando GET por id ou username
        """
        queryset = User.objects.all()
        if isinstance(kwargs.get('pk'), int) or kwargs.get('pk').isnumeric():
            user = get_object_or_404(queryset, pk=kwargs.get('pk'))
        else:
            user = get_object_or_404(queryset, username=kwargs.get('pk'))
        serializer = UserSerializer(user)
        return Response(serializer.data)


class AccountViewSet(viewsets.ReadOnlyModelViewSet):
    """ View para renderizar o usuário logado
    """
    serializer_class = AccountSerializer
    queryset = User.objects.all()
    permission_classes = [permissions.IsAuthenticated]


class UnidadeViewSet(viewsets.ModelViewSet):
    queryset = UnidadeOrganizacional.objects.all()
    serializer_class = UnidadeOrganizacionalSerializer
    permission_classes = [permissions.IsAuthenticated]


class SetorViewSet(viewsets.ModelViewSet):
    queryset = Setor.objects.all()
    serializer_class = SetorSerializer
    permission_classes = [permissions.IsAuthenticated]


class FuncaoViewSet(viewsets.ModelViewSet):
    queryset = Funcao.objects.all()
    serializer_class = FuncaoSerializer
    permission_classes = [permissions.IsAuthenticated]


class GrandeComandoViewSet(viewsets.ModelViewSet):
    queryset = GrandeComando.objects.all()
    serializer_class = GrandeComandoSerializer
    permission_classes = [permissions.IsAuthenticated]


class PlanoComissionamentoViewSet(viewsets.ModelViewSet):
    queryset = PlanoComissionamento.objects.all()
    serializer_class = PlanoComissionamentoSerializer
    permission_classes = [permissions.IsAuthenticated]


class ComissionamentoViewSet(viewsets.ModelViewSet):
    queryset = Comissionamento.objects.all()
    serializer_class = ComissionamentoSerializer
    permission_classes = [permissions.IsAuthenticated]


class MissaoViewSet(viewsets.ModelViewSet):
    queryset = Missao.objects.all()
    serializer_class = MissaoSerializer
    permission_classes = [permissions.IsAuthenticated]


class ModuloMissaoViewSet(viewsets.ModelViewSet):
    queryset = ModuloMissao.objects.all()
    serializer_class = ModuloMissaoSerializer
    permission_classes = [permissions.IsAuthenticated]
