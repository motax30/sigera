""" Arquivo default para configurar as classes dos modelos do django.
"""
import urllib.request
import json

from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.core.validators import MinValueValidator, MaxValueValidator
from django.core.exceptions import ValidationError
from django.contrib.auth.models import AbstractUser

from .choices import BRAZILIAN_STATES, DOCUMENT_LIST, CIRCULOS_HIERARQUICOS,GRUPO_ESTADO_CIDADE
from .utils.cpf_validator import validate_cpf
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles

LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted([(item, item) for item in get_all_styles()])

class GrandeComando(models.Model):
    sigla = models.CharField(max_length=15)
    nome = models.CharField(max_length=100)
    ativo = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Grande Comando"
        verbose_name_plural = "Grandes Comandos"

    def __str__(self):
        return self.sigla+" - "+ self.nome


class User(AbstractUser):
    """ Classe base para a autenticação de usuários
    """
    name = models.CharField(
        "Nome", max_length=100
    )
    email = models.EmailField(
        'Endereço de email', unique=True
    )
    cpf = models.CharField(
        "CPF", unique=True, max_length=11, validators=[validate_cpf],
        blank=True, null=True
    )
    military_rank = models.CharField(
        "Posto/Graduação", max_length=15, null=True, blank=True
    )
    military_name = models.CharField(
        "Nome de Guerra", max_length=30, null=True, blank=True
    )
    military_career = models.CharField(
        "Quadro", max_length=15, null=True, blank=True
    )
    military_speciality = models.CharField(
        "Especialidade", max_length=15, null=True, blank=True
    )
    military_subspeciality = models.CharField(
        "Subespecialidade", max_length=15, null=True, blank=True
    )
    military_id = models.CharField(
        "Número de Ordem", max_length=15, null=True, blank=True
    )
    civilian_career = models.CharField(
        "Carreira", max_length=100, null=True, blank=True
    )
    civilian_level = models.CharField(
        "Nível", max_length=5, null=True, blank=True,
    )
    civilian_position = models.CharField(
        "Cargo", max_length=30, null=True, blank=True
    )
    civilian_position_acronym = models.CharField(
        "Sigla Cargo", max_length=15, null=True, blank=True
    )
    civilian_class_reference = models.CharField(
        "Classe/Referência", max_length=50, null=True, blank=True
    )
    identity_document_type = models.CharField(
        "Tipo", max_length=30, blank=True, null=True, choices=DOCUMENT_LIST
    )
    identity_document_number = models.CharField(
        "Numero", max_length=15, blank=True, null=True
    )
    identity_document_issuer = models.CharField(
        "Orgão Emissor", max_length=15, blank=True, null=True
    )
    identity_document_uf = models.CharField(
        "UF", max_length=2, blank=True, null=True, choices=BRAZILIAN_STATES
    )
    identity_document_nationality = models.CharField(
        "País de Origem", max_length=10, blank=True, null=True
    )
    avatar = models.ImageField(
        'Avatar', blank=True, null=True
    )
    dependente = models.BooleanField(default=False, blank=True)

    def clean(self):
        """ Método para efetuar validações compostas (mais de um campo)
        """

        if self.is_military() and self.is_civilian():
            raise ValidationError(
                ('Não é possível preencher os campos de Militar ' +
                 'e Civil em um mesmo cadastro!'))

    def is_military(self):
        """ Determina se o usuário atual é militar
        """
        return self.military_rank or \
            self.military_name or \
            self.military_career or \
            self.military_speciality or \
            self.military_subspeciality

    def is_civilian(self):
        """ Determina se o usuário atual é civil
        """
        return self.civilian_career or \
            self.civilian_level or \
            self.civilian_position or \
            self.civilian_position_acronym or\
            self.civilian_class_reference

    def get_title(self):
        """
        Ex:
        - TCel QOAV
        """

        if self.is_military():
            title = self.military_rank + ' ' + self.military_career
            if self.military_speciality:
                title += ' ' + self.military_speciality
            if self.military_subspeciality:
                title += ' ' + self.military_subspeciality
            return title

        return self.civilian_career


# pylint: disable=unused-argument
@receiver(pre_save, sender=User)
def pre_save_user(sender, instance, **kwargs):
    """
    Funcao pre_save para alterar o first_name e last_name de acordo com o tipo
    de usuário:
    - se militar: posto/graduação + nome de guerra
    - se civil: posição + último nome
    - se indefinido: primeiro nome + último nome
    """

    nome_completo = instance.name.split()

    if nome_completo:
        instance.first_name = nome_completo[0]
        instance.last_name = nome_completo[-1]

    # Ajuste para militares
    if instance.military_rank and instance.military_name:
        instance.first_name = instance.military_rank
        instance.last_name = instance.military_name

    # Ajuste para civis
    elif instance.civilian_position_acronym and nome_completo:
        instance.first_name = instance.civilian_position_acronym
        instance.last_name = nome_completo[-1]


class UnidadeOrganizacional(models.Model):
    sigla = models.CharField(max_length=15)
    nome = models.CharField(max_length=100)
    ativo = models.BooleanField(default=True)
    grandeComando = models.ForeignKey(GrandeComando, related_name = "unidades", on_delete = models.CASCADE)
    
    class Meta:
        verbose_name = "Unidade"
        verbose_name_plural = "Unidades"
    
    def __str__(self):
        return self.sigla+" - "+ self.nome

    def getSigla(self):
        return self.sigla

    def getNome(self):
        return self.nome

class Setor(models.Model):
    """ Classe de configuração do modelo Setor.
    """
    unidade_organizacional = models.ForeignKey(
        UnidadeOrganizacional, verbose_name='Unidade Organizacional',
        blank=True, null=True, on_delete=models.SET_NULL
    )
    parent = models.ForeignKey(
        'Setor', verbose_name='Setor pai', on_delete=models.CASCADE,
        blank=True, null=True
    )
    initials = models.CharField(
        'Sigla', max_length=10
    )
    description = models.CharField(
        'Descrição', max_length=200
    )
    active = models.BooleanField(
        'Ativo', default=True
    )
    created = models.DateTimeField(
        'Criação', auto_now_add=True
    )
    modified = models.DateTimeField(
        'Modificação', auto_now=True
    )

    def __str__(self):
        if self.unidade_organizacional:
            # pylint: disable=no-member
            return "%s / %s" % (
                self.unidade_organizacional.sigla, self.initials
            )
        return "- / %s" % self.initials

    # pylint: disable=too-few-public-methods
    class Meta:
        """ Classe para alterar os names das colunas.
        """
        verbose_name = 'Setor'
        verbose_name_plural = 'Setores'
        unique_together = [
            ['unidade_organizacional', 'initials']
        ]


# pylint: disable=unused-argument
@receiver(pre_save, sender=Setor)
def pre_save_setor(sender, instance, **kwargs):
    """ Função executada antes da persistência em banco toda vez que um
        Setor é criado ou alterado.
    """
    if instance.parent is not None:
        instance.unidade_organizacional = \
            instance.parent.unidade_organizacional


# pylint: disable=unused-argument
@receiver(post_save, sender=Setor)
def post_save_setor(sender, instance, **kwargs):
    """ Função executada após a persistência em banco toda vez que um
        Setor é alterado.
    """
    # Call children's pre_save hook, which updates the unidade_organizacional
    # according to their parents
    for child in Setor.objects.filter(parent=instance):
        child.save()


class Funcao(models.Model):
    """ Classe de configuração do modelo de Função.
    """
    short_name = models.CharField(
        "Nome da Função Abreviada", max_length=255
    )
    long_name = models.CharField(
        "Nome da Função por Extenso", max_length=255
    )
    initials = models.CharField(
        "Sigla da Função", max_length=15
    )
    setor = models.ForeignKey(
        Setor, verbose_name='Setor', on_delete=models.CASCADE,
        blank=True, null=True
    )
    user = models.ForeignKey(
        User, verbose_name='Usuário', on_delete=models.SET_NULL,
        blank=True, null=True
    )
    active = models.BooleanField(
        'Ativa', default=True
    )
    created = models.DateTimeField(
        'Criação', auto_now_add=True
    )
    modified = models.DateTimeField(
        'Modificação', auto_now=True
    )

    def __str__(self):
        return "%s" % self.initials

    # pylint: disable=too-few-public-methods
    class Meta:
        """ Classe para alterar os names das colunas.
        """
        verbose_name = 'Função'
        verbose_name_plural = 'Funções'

class PlanoComissionamento(models.Model):
    ano = models.IntegerField()
    unidade = models.OneToOneField(UnidadeOrganizacional, on_delete = models.CASCADE)
    valor_total_previsto = models.FloatField()
    valor_total_executado = models.FloatField()

    def getAno(self):
        return self.ano

    class Meta:
        verbose_name = "Plano de Comissionamento"
        verbose_name_plural = "Planos de Comissionamento"

    def __str__(self):
        return "%s %s %s %d" %("Plano de Comissionamento do ", self.unidade,"do ano de ",self.ano)

class Localidade(models.Model):
    cidade = models.CharField(max_length=255)
    uf = models.CharField(max_length=2)

    def __str__(self):
        return "%s%s%s"%(self.cidade," - ",self.uf)
    

class Comissionamento(models.Model):
    responsavel_credito = models.ForeignKey(UnidadeOrganizacional, on_delete=models.CASCADE,
    related_name='resp_credito', blank=True, null=True)
    ug_responsavel = models.ForeignKey(UnidadeOrganizacional, on_delete=models.CASCADE,
    related_name='ug_resp', blank=True, null=True)
    ug_pagadora = models.ForeignKey(UnidadeOrganizacional, on_delete=models.CASCADE,
    related_name='ug_pag', blank=True, null=True)
    inicio = models.DateField()
    termino = models.DateField()
    previsao_promocao = models.DateField(verbose_name="Previsão Promoção")
    qtd_dias_planejados = models.IntegerField(verbose_name="Quantidade de Dias Planejados", default=0)
    qtd_dias_executados = models.IntegerField(verbose_name="Quantidade de Dias Executados", default=0)
    mes_pagamento = models.CharField(max_length=255, blank=True, null=True)
    comp_ano = models.DecimalField(verbose_name="Ajuda de Custo Comprometida no Ano",
                                    decimal_places=2, max_digits=8, blank=True, null=True)
    comp_ano_seguinte = models.DecimalField(verbose_name="Ajuda de Custo Comprometida no Ano Seguinte",
                                            decimal_places=2, max_digits=8, blank=True, null=True)
    valor_ida_planejado = models.DecimalField(decimal_places=2, max_digits=8, blank=True, null=True)
    valor_volta_planejado = models.DecimalField(decimal_places=2, max_digits=8, blank=True, null=True)
    valor_pago_ida = models.DecimalField(decimal_places=2, max_digits=8, blank=True, null=True)
    mes_contracheque_ida = models.CharField(max_length=30, blank=True, null=True)
    valor_pago_volta = models.DecimalField(decimal_places=2, max_digits=8, blank=True, null=True)
    mes_contracheque_volta = models.CharField(max_length=30, blank=True, null=True)
    unidade = models.ForeignKey(UnidadeOrganizacional, on_delete = models.CASCADE)
    user = models.ForeignKey(User, verbose_name="user",on_delete = models.CASCADE)
    plano = models.OneToOneField(PlanoComissionamento, on_delete = models.CASCADE)

    class Meta:
        verbose_name = "Comissionamento"
        verbose_name_plural = "Comissionamentos"

    def __str__(self):
        return '%d%s%s%s%d'%(
            self.id,'/',
            self.plano.unidade.getSigla(),
            '/',
            self.plano.getAno())

class Missao(models.Model):
    descricao = models.CharField(max_length=255,verbose_name = "Natureza da Missão")
    user=models.ForeignKey(User, on_delete = models.CASCADE, blank=True, null=True)
    valorTotalAjudaCusto = models.DecimalField(verbose_name = "Ajuda de Custo (Valor Total)",max_digits=8, decimal_places=2)
    valorTotalDiarias = models.DecimalField(verbose_name = "Diárias (Valor Total)",max_digits=8, decimal_places=2)
    comissionamento = models.ForeignKey(Comissionamento, on_delete = models.CASCADE, blank=True, null=True)
    valor_ida = models.DecimalField(verbose_name = "Ajuda de Custo (Valor Ida)",max_digits=8, decimal_places=2)
    valor_volta = models.DecimalField(verbose_name = "Ajuda de Custo (Valor Volta)",max_digits=8, decimal_places=2)

    class Meta:
        verbose_name = "Missão"
        verbose_name_plural = "Missões"

    def __str__(self):
        return "%s"%(self.descricao)
        
class ClasseDiaria(models.Model):
    circulo_hierarquico = models.CharField(max_length=255, choices=CIRCULOS_HIERARQUICOS)
    grupo_estado_cidade = models.CharField(max_length=255, choices=GRUPO_ESTADO_CIDADE)
    valor = models.DecimalField(decimal_places=2, max_digits=8, verbose_name="Valor de Diária por localidade")

    class Meta:
        verbose_name_plural = "Classes Diárias"

    def __str__(self):
        return "%s%s%s"%(self.circulo_hierarquico,"-",self.grupo_estado_cidade)

class ModuloMissao(models.Model):
    inicio = models.DateField()
    termino = models.DateField()
    ordem_servico = models.IntegerField()
    #unidade = models.ForeignKey(UnidadeOrganizacional, on_delete = models.CASCADE)
    descricao = models.CharField(max_length=255)
    missao = models.ForeignKey(Missao, on_delete = models.CASCADE, blank=True, null=True)
    comissionamento = models.ForeignKey(Comissionamento, on_delete = models.CASCADE, blank=True, null=True)
    ano = models.IntegerField()
    localidade = models.OneToOneField(Localidade,on_delete = models.CASCADE)
    valor = models.DecimalField(max_digits=8, decimal_places=2)
    quantidade_dias = models.IntegerField()
    quantidade_diarias = models.IntegerField()
    classe_da_diaria = models.ForeignKey(ClasseDiaria,on_delete = models.CASCADE, blank=True, null=True)
    class Meta:
        verbose_name = "Modulo Missão"
        verbose_name_plural = "Módulos de Missão"
