""" Configurações de URLs para o app sigcom
"""
from django.urls import include, path
from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView

from . import views

router = routers.DefaultRouter()
router.register('users', views.UserViewSet, basename='users')
router.register('account', views.AccountViewSet)
router.register('unidades', views.UnidadeViewSet)
router.register('Setor', views.SetorViewSet)
router.register('Funcao', views.FuncaoViewSet)
router.register('grandescomandos', views.GrandeComandoViewSet)
router.register('planos', views.PlanoComissionamentoViewSet)
router.register('comissionamentos', views.ComissionamentoViewSet)
router.register('missoes', views.MissaoViewSet)
router.register('modulos', views.ModuloMissaoViewSet)
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path(
        'token-auth/',
        views.TokenObtainPairPatchedView.as_view(),
        name='token-auth'),
    path('token-refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('auth/', include('rest_framework.urls'))
]