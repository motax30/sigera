from django.contrib import admin
from .models import GrandeComando, UnidadeOrganizacional, PlanoComissionamento, User, Localidade, ModuloMissao, Missao, Comissionamento
from core.models import ClasseDiaria, Funcao, Setor

admin.site.site_header = 'SIGCOM - Sistema de Gerenciamento de Comissionamentos'

class UnidadeInline(admin.TabularInline):
    model = UnidadeOrganizacional
    extra = 1
    verbose_name = "Unidade"
    verbose_name_plural = "Unidades"

class ComissionamentoInLine(admin.TabularInline):
    model = Comissionamento
    extra = 1
    verbose_name = "Comissionamento"
    verbose_name_plural = "Comissionamentos"
    fieldsets = (
        (None, {
            'fields': (
                'inicio','termino','previsao_promocao','qtd_dias_planejados',
                'qtd_dias_executados','user','mes_pagamento','unidade'
            ),
        }),
    )

class GrandeComandoAdmin(admin.ModelAdmin):
    list_display = ['sigla','nome','ativo']
    ordering = ['sigla']
    fieldsets = [
        (None,               {'fields': ['sigla']}),
        ('Dados do Grande Comando', {'fields': ['nome','ativo']}),
    ]
    inlines = [UnidadeInline]
    search_fields = ['sigla','nome']

class UnidadeAdmin(admin.ModelAdmin):
    list_display = ['sigla','nome','ativo','grandeComando']
    ordering = ['sigla']
    fieldsets = [
        (None,               {'fields': ['sigla']}),
        ('Dados da Unidade', {'fields': ['nome','ativo','grandeComando']}),
    ]
    search_fields = ['sigla','nome']

class ModuloMissaoInLine(admin.TabularInline):
    model = ModuloMissao
    extra = 1
    verbose_name = "Módulo de Missão"
    verbose_name_plural = "Módulo(s) de Missão"
    fieldsets = (
        (None, {
            'fields': (
                'inicio','termino','ordem_servico','descricao','localidade','valor',
                'quantidade_dias','quantidade_diarias','classe_da_diaria'
            ),
        }),
    )

class MissaoAdmin(admin.ModelAdmin):
    list_display = ['descricao','user','valorTotalAjudaCusto','valorTotalDiarias','comissionamento']
    fieldsets = [
        ('Militares(es) ou Civil(s) envolvido(s)',{'fields':['user']}),
        ('Dados da Missão',{'fields': ['descricao','valor_ida','valor_volta','valorTotalAjudaCusto','valorTotalDiarias']}),
    ]
    inlines = [ModuloMissaoInLine]

class MissaoInLine(admin.TabularInline):
    model = Missao
    extra = 1
    verbose_name = "Missão"
    verbose_name_plural = "Missões"
    exclude = ('user',)

class ComissioanamentoAdmin(admin.ModelAdmin):
    list_display = ['user','inicio','termino','plano', 'previsao_promocao']
    fieldsets = [
        ('Período Total',{'fields': ['inicio','termino']}),
        ('Dados Gerais do Comissionamento', {'fields': [
            'plano', 'user', 'unidade', 'previsao_promocao',
            'responsavel_credito','ug_responsavel',
            'qtd_dias_planejados', 'qtd_dias_executados']}),
    ]
    inlines = [MissaoInLine, ModuloMissaoInLine]

class PlanoAdmin(admin.ModelAdmin):
    extra = 1
    inlines = [ComissionamentoInLine]

class MilitarAdmin(admin.ModelAdmin):
    list_display = ['',]

class FuncaoInLine(admin.TabularInline):
    """ Inline do modelo função
    """
    model = Funcao
    extra = 3


class SetorInline(admin.TabularInline):
    """ Inline do modelo setor
    """
    model = Setor
    extra = 3
    exclude = ['unidade_organizacional']
    verbose_name = "Setor Subordinado"
    verbose_name_plural = "Setores Subordinados"


class SetorAdmin(admin.ModelAdmin):
    """ Classe de configuração do modelo Setor.
    """
    list_display = [
        'initials', 'description', 'active', 'parent',
        'unidade_organizacional'
    ]
    list_filter = ['unidade_organizacional', 'active']
    search_fields = ['initials', 'description']
    raw_id_fields = ('parent',)
    inlines = [FuncaoInLine, SetorInline]


class FuncaoAdmin(admin.ModelAdmin):
    """ Classe de configuração do modelo Função.
    """
    list_display = [
        'id', 'initials', 'long_name', 'user', 'active', 'setor'
    ]
    list_filter = ['setor__unidade_organizacional', 'active']
    search_fields = ['short_name', 'long_name', 'initials']
    raw_id_fields = ('setor', 'user')

class ClasseDiariaAdmin(admin.ModelAdmin):
    """Classe que define os Circulos Hierárquicos"""
    list_display = [
        'id','circulo_hierarquico','grupo_estado_cidade','valor'
    ]

admin.site.register(GrandeComando, GrandeComandoAdmin)
admin.site.register(Comissionamento, ComissioanamentoAdmin)
admin.site.register(User)
admin.site.register(PlanoComissionamento, PlanoAdmin)
admin.site.register(UnidadeOrganizacional, UnidadeAdmin)
admin.site.register(Missao, MissaoAdmin)
admin.site.register(Localidade)
admin.site.register(ModuloMissao)
admin.site.register(Setor, SetorAdmin)
admin.site.register(Funcao, FuncaoAdmin)
admin.site.register(ClasseDiaria, ClasseDiariaAdmin)