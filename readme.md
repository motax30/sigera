# SIGCOM - SISTEMA DE GERENCIAMENTO DE COMISSIONAMENTOS

Este sistema foi concebido para prover o gerencimento de rotinas administrativas do Setor de Recursos Humanos de uma unidade militar pertencente ao Comando da Aeronáutica,
tendo aplicação para a resolução de processos do cotidiano daquele setor. Como primeiro módulo, está sendo desenvolvido o Controle do Planejamento e Execução de diferentes eventos,
ora denominados MISSÔES, atividades estas realizadas por militares, para atendimento às demandas operacionais da unidade militar a que estes militares estão subordinados. 
Para o custeio das referidas missões, a unidade militar efetua o pagamento de Ajudas de Custo aos militares que as realizarão e, a este processo de planejamento, execução e pagamento denominamos Comissionamento.

## Tecnologias utilizadas na Aplicação

1. **Linguagem de Programação**: Python 3x
2. **Framework de Desenvolvimento do BackEnd**: Django 3x
3. **Módulo 'Suit' para Django**: Responsável pelo layout geral das Telas do Sistema
Site documentação do Suit: <https://django-suit.readthedocs.io/en/develop/configuration.html>
4. **Banco de Dados**: Postgres

## Pré Requisitos

1. **Sistema Operacional**: Windows, Linux, Mac

## Instalação e Operação

### Backend

>#### Baixando a aplicação
> Em um diretório de sua escolha digite o seguinte comando para baixar o código da aplicação:
>-`git clone https://gitlab.com/motax30/sigera.git`
>
>Obs.: Caso ocorra algum problema ao baixar a aplicação retire o caracter 's' de 'https' na url, pois o 's' na Url significa que as informações estão trafegando em um protocolo seguro. Em alguns casos você pode não conseguir baixar o código por esse motivo.
>
>

 - Após baixar a aplicação, navegue para a pasta onde foi baixada a aplicação em seu computador e efetue os seguintEs procedimentos:

>INSTALAÇÃO DO VIRTUALENV
>>
>>**VirtualEnv** - Aplicação que isola a aplicação bem como instalações das diferentes bibliotecas utilizadas na aplicação para que não haja interferências do sistema operacional hospedeiro da aplicação.
>>
>>_Passos para instalação:_
>>
>>**1- Instalação do Virtualenv via Pip**
>>
>>`python -m pip install --user virtualenv`
>>
>>**Efetuando a criação do ambiente virtual onde a aplicação será executada**
>>
>>`virtualenv venv`
>>
>>**Ativando o ambiente virtual**
>>
>>`source venv/Scripts/activate`

 - Baixando as bibliotecas necessárias para o correto funcionamento da aplicação backend.
    
    `pip install -r requirements.txt`

- Criação do super usuário para acesso à Interface Administrativa. Escolher as opções de usuário,email e senha e confirmar.

    `python manage.py createsuperuser` 

- Comando responsável por montar todos os arquivos de migração das alterações de tabelas no banco de dados.
    
    `python manage.py makemigrations` 

-  Comando para aplicação das migrações anteriormente construidas.
    
     `python manage.py migrate` 

- Rodando a aplicação BackEnd
    
     `python manage.py runserver` 

A URL para acessar a interface administrativa é: <http://localhost:8000/admin>

Tela de Login Inteface Administrativa do Django

![Tela de Login Inteface Administrativa](frontend/src/assets/images/readme/tela_login_django_admin.png "Tela de Login Inteface Administrativa")

Informar as seguintes informações na tela de login que se abrirá:
**Usuário**: admin
(Supondo que este usuario tenha sido definido para a interface administrativa)
**Senha**: admin
(Supondo que esta senha tenha sido definida para a interface administrativa)

Home da Inteface Administrativa Django

![Home da Inteface Administrativa Django](frontend/src/assets/images/readme/home_django_admin.png "Home da Inteface Administrativa Django")

### Frontend

Para subirmos a aplicação Frontend estando na pasta raiz onde foi feito o checkout do projeto, execute o seguinte comando

`cd frontend` -  *Com este comando acessaremos a pasta onde se encontram os arquivos utilizados no projeto frontend*

Estando na pasta 'frontend' e Uma vez instalado e ativado ambiente virtual do VirtualEnv efetue a instalação do NODEJS, conforme a seguir.

**NODEJS** - Plataforma construída sobre o motor JavaScript do Google Chrome para facilmente construir aplicações de rede rápidas e escaláveis. Node.js usa um modelo de I/O direcionada a evento não bloqueante que o torna leve e eficiente, ideal para aplicações em tempo real com troca intensa de dados através de dispositivos distribuídos. (Fonte: http://nodebr.com/o-que-e-node-js/)

Passos para instalação do Node na **Virtualenv**:

1 - `pip install nodeenv `

2 - `nodeenv -p`  

Atualizando as bibliotecas do NPM para a correta execução da aplicação:

`npm install`

Subindo a aplicação Frontend

`npm run server`

Após a execução do comando anterior a aplicação será disponibilizada no seguinte endereço: <http://localhost:4200>

Tela de Login do SIGCOM

![Tela de Login do SIGCOM](frontend/src/assets/images/readme/tela_login_sigcom.png "Tela de Login do SIGCOM")

#### Manual de utilização

Em desenvolvimento..

#### Procedimentos para geração do arquivo de UML da aplicação utilizando a biblioteca (Graph models)

<https://django-extensions.readthedocs.io/en/latest/graph_models.html#example-usage>
