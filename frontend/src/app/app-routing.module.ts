import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './modules/auth/login/login.component';
import { UserRouteAccessService } from './modules/auth/auth/user-route-access-service';


const routes: Routes = [
  {path: 'login', component: LoginComponent },
  {path: '', loadChildren: () => import('./modules/pages/pages.module').then(m => m.PagesModule),
  canActivate: [UserRouteAccessService]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
