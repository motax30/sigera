import { environment } from 'src/environments/environment';

export const SERVER_API_URL = environment.apiBaseUrl;
export const USER_API_URL = SERVER_API_URL + 'api/users/';
export const COMISSIONAMENTO_API_URL = SERVER_API_URL + 'api/comissionamentos/';
