export interface ItemMenu {
    nome: string;
    rota: string;
}

export interface ItemNav {
    nome: string;
    subitens: ItemMenu[];
}

export const itensNav: ItemNav[] = [
    {
        nome: 'Cadastros',
        subitens: [
            { nome: 'Users', rota: 'users' },
            { nome: 'Grandes Comandos', rota: 'comandos' },
            { nome: 'Unidades', rota: 'unidades' }
        ]
    },
    {
        nome: 'Planejamento',
        subitens: [{ nome: 'Planos', rota: 'planos' }]
    },
    {
        nome: 'Execução',
        subitens: [{ nome: 'Comissionamentos', rota: 'comissionamentos' }]
    }
];
