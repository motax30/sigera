import { Component, OnInit } from '@angular/core';
import { itensNav, ItemNav } from '../item-menu';

declare var $;

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  itens: ItemNav[];

  constructor() {
  }

  ngOnInit() {
    $(document).ready(() => {
      $('.sidebar-menu').tree();
    });
    this.itens = itensNav;
  }

}
