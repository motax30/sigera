import { LoginService } from './../../auth/auth/login.service';
import { LoginComponent } from './../../auth/login/login.component';
import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import { ItemNav } from '../item-menu';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();
  itens: ItemNav[];

  constructor(private loginService: LoginService, private router: Router) {
  }

  ngOnInit() {
  }

  toggleSideBar() {
    this.toggleSideBarForMe.emit();
    setTimeout(() => {
      window.dispatchEvent(
        new Event('resize')
      );
    }, 300);
  }

  logout() {
    this.loginService.logout();
    this.router.navigate(['/login']);
  }

}
