export class Account {
    constructor(
        public id: number,
        public isActive: boolean,
        public groups: string[],
        public email: string,
        public firstName: string,
        public lastName: string,
        public username: string,
        public avatar: string
    ) { }
}
