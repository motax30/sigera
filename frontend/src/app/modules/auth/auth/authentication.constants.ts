export const ACESS_TOKEN = 'accessToken';
export const REFRESH_TOKEN = 'refreshToken';
export const EXPIRE_AT = 'expiresAt';
export const REMEMBER_ME = 'rememberMe';
export const CURRENT_USER = 'currentUser';
