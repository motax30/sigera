import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { SERVER_API_URL } from 'src/app/app.constants';
import { User } from '../../pages/user/user.interface';
import { CURRENT_USER, ACESS_TOKEN, REFRESH_TOKEN, EXPIRE_AT } from './authentication.constants';

@Injectable({ providedIn: 'root' })
export class LoginService {

  private resourceUrl =  SERVER_API_URL + 'api/';
  private currentUserSubject: BehaviorSubject<User>
  public currentUser: Observable<User>;

  constructor(
    private http: HttpClient,
    private $localStorage: LocalStorageService,
    private $sessionStorage: SessionStorageService
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem(CURRENT_USER)));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public getCurrentUserValue(): User {
    return this.currentUserSubject.value;
  }

  refreshToken() {
    return this.http.post(this.resourceUrl + 'token-refresh/', {
        refresh: this.getRefreshToken()
    }).pipe(tap((token: Token) => {
        this.storeAuthenticationToken(
            token.access,
            this.getRefreshToken(),
        );
    }));
  }

  login(credentials: any): Observable<User> {

    const data = {
      username: credentials.username,
      password: credentials.password,
      rememberMe: credentials.rememberMe
    };

    return this.http.post(this.resourceUrl + 'token-auth/', data).pipe(
      map(res => {
        const token = res as Token;
        const accessToken = token.access;
        const refreshToken = token.refresh;
        const user = token.user;

        this.storeAuthenticationToken(accessToken, refreshToken, credentials.rememberMe, user);

        localStorage.setItem(CURRENT_USER, JSON.stringify(user));
        this.currentUserSubject.next(user);

        return user;
      })
    )
  }

  logout() {
    this.currentUserSubject.next(null);

    localStorage.removeItem(ACESS_TOKEN);
    localStorage.removeItem(REFRESH_TOKEN);
    localStorage.removeItem(EXPIRE_AT);
    localStorage.removeItem(CURRENT_USER);

    sessionStorage.removeItem(ACESS_TOKEN);
    sessionStorage.removeItem(REFRESH_TOKEN);
    sessionStorage.removeItem(EXPIRE_AT);
    sessionStorage.removeItem(CURRENT_USER);
  }

  isLoggedIn() {
    return this.getCurrentUserValue() !== null;
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getToken() {
    return this.$localStorage.retrieve(ACESS_TOKEN) || this.$sessionStorage.retrieve(ACESS_TOKEN);
  }

  getRefreshToken() {
    return this.$localStorage.retrieve(REFRESH_TOKEN) || this.$sessionStorage.retrieve(REFRESH_TOKEN);
  }

  getExpiration() {
    const expiration = this.$localStorage.retrieve(EXPIRE_AT) || this.$sessionStorage.retrieve(EXPIRE_AT);
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }

  private storeAuthenticationToken(accessToken: string, refreshToken: string, rememberMe?: boolean, user?: User) {
    const accessTokenParts = accessToken.split(/\./);
    const accessTokenDecoded = JSON.parse(window.atob(accessTokenParts[1]));
    const expiresAt = moment.unix(accessTokenDecoded.exp);

    // Se o rememberMe não for definido, verificar se existe algo no localStorage,
    // o que significa que o usuário escolhou o rememberMe no login
    if (typeof rememberMe === 'undefined' || rememberMe === null) {
      if (this.$localStorage.retrieve(CURRENT_USER)) {
        rememberMe = true;
      } else {
        rememberMe = false;
      }
    }

    if (rememberMe) {
      this.$localStorage.store(ACESS_TOKEN, accessToken);
      this.$localStorage.store(REFRESH_TOKEN, refreshToken);
      this.$localStorage.store(EXPIRE_AT, JSON.stringify(expiresAt.valueOf()));
      if (user) {
        this.$localStorage.store(CURRENT_USER, user);
      }
    } else {
      this.$sessionStorage.store(ACESS_TOKEN, accessToken);
      this.$sessionStorage.store(REFRESH_TOKEN, refreshToken);
      this.$sessionStorage.store(EXPIRE_AT, JSON.stringify(expiresAt.valueOf()));
      if (user) {
        this.$sessionStorage.store(CURRENT_USER, user);
      }
    }
  }

}

export class Token {
  access: string;
  refresh: string;
  user: User;
}
