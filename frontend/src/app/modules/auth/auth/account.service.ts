import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { shareReplay, tap, catchError } from 'rxjs/operators';

import { Account } from './account.model';
import { SERVER_API_URL } from 'src/app/app.constants';


@Injectable({ providedIn: 'root' })
export class AccountService {
  private userIdentity: Account;
  private authenticated = false;
  private authenticationState = new Subject<any>();
  private accountCache$: Observable<Account>;

  constructor(private http: HttpClient) { }

  fetch(username): Observable<Account> {
    return this.http.get<Account>(SERVER_API_URL + 'api/account/' + username);
  }

  save(account: Account): Observable<Account> {
    return this.http.post<Account>(SERVER_API_URL + 'api/account/', account);
  }

  authenticate(identity) {
    this.userIdentity = identity;
    this.authenticated = identity !== null;
    this.authenticationState.next(this.userIdentity);
  }

  hasAnyAuthority(groups: string[] | string): boolean {
    if (!this.authenticated || !this.userIdentity || !this.userIdentity.groups) {
      return false;
    }

    if (!Array.isArray(groups)) {
      groups = [groups];
    }

    return groups.some((group: string) => this.userIdentity.groups.includes(group));
  }

  identity(force?: boolean, _username?: string): Observable<Account> {
    if (force || !this.authenticated) {
      this.accountCache$ = null;
    }

    if (!this.accountCache$) {
      this.accountCache$ = this.fetch(_username).pipe(
        catchError(() => {
          return of(null);
        }),
        tap(account => {
          if (account) {
            this.userIdentity = account;
            this.authenticated = true;
          } else {
            this.userIdentity = null;
            this.authenticated = false;
          }
          this.authenticationState.next(this.userIdentity);
        }),
        shareReplay()
      );
    }
    return this.accountCache$;
  }

  isAuthenticated(): boolean {
    return this.authenticated;
  }

  isIdentityResolved(): boolean {
    return this.userIdentity !== undefined;
  }

  getAuthenticationState(): Observable<any> {
    return this.authenticationState.asObservable();
  }

  getImageUrl(): string {
    return this.isIdentityResolved() ? this.userIdentity.avatar : null;
  }

  getUserIdentity(): Account {
    return this.isIdentityResolved() ? this.userIdentity : null;
  }
}
