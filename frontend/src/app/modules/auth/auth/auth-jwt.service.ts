import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';

import * as moment from 'moment';
import { SERVER_API_URL } from '../../../app.constants';


@Injectable({ providedIn: 'root' })
export class AuthServerProvider {

    private readonly ACESS_TOKEN = 'accessToken';
    private readonly REFRESH_TOKEN = 'refreshToken';
    private readonly EXPIRE_AT = 'expiresAt';
    private readonly REMEMBER_ME = 'rememberMe';

    constructor(private http: HttpClient, private $localStorage: LocalStorageService, private $sessionStorage: SessionStorageService) { }

    getToken() {
        return this.$localStorage.retrieve(this.ACESS_TOKEN) || this.$sessionStorage.retrieve(this.ACESS_TOKEN);
    }

    getRefreshToken() {
        return this.$localStorage.retrieve(this.REFRESH_TOKEN) || this.$sessionStorage.retrieve(this.REFRESH_TOKEN);
    }

    getExpiration() {
        const expiration = this.$localStorage.retrieve(this.EXPIRE_AT) || this.$sessionStorage.retrieve(this.EXPIRE_AT);
        const expiresAt = JSON.parse(expiration);
        return moment(expiresAt);
    }

    getRememberMe() {
        return this.$localStorage.retrieve(this.REMEMBER_ME) || this.$sessionStorage.retrieve(this.REMEMBER_ME);
    }

    login(credentials: any): Observable<any> {
        const data = {
            username: credentials.username,
            password: credentials.password,
            rememberMe: credentials.rememberMe
        };

        function authenticateSuccess(resp) {
            const accessToken = resp.body.access;
            const refreshToken = resp.body.refresh;

            this.storeAuthenticationToken(accessToken, refreshToken, credentials.rememberMe);

            return accessToken;
        }

        return this.http.post(SERVER_API_URL + 'api/token-auth/', data, { observe: 'response' })
            .pipe(map(authenticateSuccess.bind(this)));
    }

    storeAuthenticationToken(accessToken: string, refreshToken: string, rememberMe: boolean) {
        const accessTokenParts = accessToken.split(/\./);
        const accessTokenDecoded = JSON.parse(window.atob(accessTokenParts[1]));
        const expiresAt = moment.unix(accessTokenDecoded.exp);

        if (rememberMe) {
            this.$localStorage.store(this.ACESS_TOKEN, accessToken);
            this.$localStorage.store(this.REFRESH_TOKEN, refreshToken);
            this.$localStorage.store(this.EXPIRE_AT, JSON.stringify(expiresAt.valueOf()));
            this.$localStorage.store(this.REMEMBER_ME, rememberMe);
        } else {
            this.$sessionStorage.store(this.ACESS_TOKEN, accessToken);
            this.$sessionStorage.store(this.REFRESH_TOKEN, refreshToken);
            this.$sessionStorage.store(this.EXPIRE_AT, JSON.stringify(expiresAt.valueOf()));
            this.$localStorage.store(this.REMEMBER_ME, rememberMe);
        }

    }

    refreshToken() {
        return this.http.post(SERVER_API_URL + 'api/token-refresh/', {
            refresh: this.getRefreshToken()
        }).pipe(tap((tokens: Tokens) => {
            this.storeAuthenticationToken(
                tokens.access,
                this.getRefreshToken(),
                this.getRememberMe()
            );
        }));
    }

    logout(): Observable<any> {
        return new Observable(observer => {
            this.$localStorage.clear(this.ACESS_TOKEN);
            this.$localStorage.clear(this.REFRESH_TOKEN);
            this.$localStorage.clear(this.EXPIRE_AT);
            this.$localStorage.clear(this.REMEMBER_ME);

            this.$sessionStorage.clear(this.ACESS_TOKEN);
            this.$sessionStorage.clear(this.REFRESH_TOKEN);
            this.$sessionStorage.clear(this.EXPIRE_AT);
            this.$sessionStorage.clear(this.REMEMBER_ME);

            observer.complete();
        });
    }
}

export class Tokens {
    access: string;
    refresh: string;
}
