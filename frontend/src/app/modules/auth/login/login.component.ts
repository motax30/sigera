import {Component, OnInit} from '@angular/core';
import { LoginService, Account } from '../auth';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  authenticationError: boolean;
  currentAccount: Account;
  returnUrl = '';

  loginForm = this.fb.group({
    username: [''],
    password: [''],
    rememberMe: [false]
  });

  constructor(
    private loginService: LoginService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    this.route.queryParams.subscribe(params => this.returnUrl = params.returnUrl || '/');

    if(this.loginService.isLoggedIn()) {
      this.currentAccount = this.loginService.getCurrentUserValue();
    }
  }

  login() {
    this.loginService
      .login({
        username: this.loginForm.get('username').value,
        password: this.loginForm.get('password').value,
        rememberMe: this.loginForm.get('rememberMe').value
      })
      .subscribe(
        (user) => {
          this.authenticationError = false;
          this.currentAccount = user;
          this.router.navigateByUrl(this.returnUrl);
        },
        () => (this.authenticationError = true)
      );
  }

  logout() {
    this.loginService.logout();
    this.router.navigate(['/login']);
  }

  isAuthenticated() {
    return this.loginService.isLoggedIn();
  }

}
