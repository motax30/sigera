import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

export abstract class EntityService<T> {

  constructor(
    protected http: HttpClient,
    protected resourceUrl: string
  ) {}

  save(t: T): Observable<T> {
    return this.http.post<T>(this.resourceUrl, t);
  }

  update(id: any, t: T): Observable<T> {
    return this.http.put<T>(this.resourceUrl + id + '/', t, {});
  }

  findOne(id: any): Observable<T> {
    return this.http.get<T>(this.resourceUrl + id + '/');
  }

  findMany(searchParams?: any): Observable<T[]> {
    const options = this.createRequestOption(searchParams);
    return this.http.get<T[]>(this.resourceUrl, { params: options });
  }

  findAll(): Observable<T[]> {
    return this.http.get<T[]>(this.resourceUrl)
  }

  delete(id: any): Observable<T> {
    return this.http.delete<T>(this.resourceUrl + id + '/');
  }

  private createRequestOption(req?: any): HttpParams {
    let options: HttpParams = new HttpParams();
    if (req) {
      Object.keys(req).forEach((key) => {
        options = options.set(key, req[key]);
        });
    }
    return options;
  }

}
