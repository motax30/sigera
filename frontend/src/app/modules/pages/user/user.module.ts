import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';
import { AngularMaterialModule } from './../../shared/angular-material.module';
import { UserDetalheComponent } from './user-detalhe/user-detalhe.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserRoutingModule } from './user-routing/user-routing.module';
import { UserComponent } from './user.component';
import { UserService } from './user.service';
@NgModule({
  declarations: [UserComponent, UserFormComponent, UserDetalheComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UserRoutingModule,
    RouterModule,
    AngularMaterialModule
  ],
  providers: [UserService],
  exports: [UserComponent, UserFormComponent, UserDetalheComponent, MatListModule]
})
export class UserModule { }
