export class User {

    constructor(
        public id?: number,
        public username?: string,
        public firstName?: string,
        public lastName?: string,
        public email?: string,
        public groups?: any[],
        public userPermissions?: any[],
        public isStaff?: boolean,
        public isActive?: boolean,
        public isSuperuser?: boolean,
        public lastLogin?: any,
        public dateJoined?: any,

        public name?: string,
        public cpf?: string,
        public militaryRank?: string,
        public militaryName?: string,
        public militaryCareer?: string,
        public militarySpeciality?: string,
        public militarySubspeciality?: string,
        public militaryId?: string,
        public civilianCareer?: string,
        public civilianLevel?: string,
        public civilianPosition?: string,
        public civilianPositionAcronym?: string,
        public civilianClassReference?: string,
        public identityDocumentType?: string,
        public identityDocumentNumber?: string,
        public identityDocumentIssuermail?: string,
        public identityDocumentUf?: string,
        public identityDocumentNationality?: string,
        public provedorLDAP?: object,
        public ldapUid?: string,
        public avatar?: string,
    ) {

    }

}
