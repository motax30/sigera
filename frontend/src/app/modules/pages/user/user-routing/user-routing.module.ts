import { NgModule } from '@angular/core';
import { UserComponent } from '../user.component';
import { RouterModule } from '@angular/router';
import { UserFormComponent } from '../user-form/user-form.component';
import { UserDetalheComponent } from '../user-detalhe/user-detalhe.component';

const userRoutes = [
  { path: '', component: UserComponent },
  { path: ':id', component: UserDetalheComponent },
  { path: ':saram/edit', component: UserFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
