import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthServerProvider } from '../../auth/auth';
import { EntityService } from '../../shared/services/entity.service';
import { USER_API_URL } from './../../../app.constants';
import { User } from './user.interface';

@Injectable()
export class UserService extends EntityService<User>{

  user: BehaviorSubject<User> = new BehaviorSubject(null);

  constructor(protected http: HttpClient) {
    super(http, USER_API_URL);
  }

  setUser(id: number) {
    this.findOne(id).subscribe(observer =>
      this.user.next(observer)
    );
  }
}
