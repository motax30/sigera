import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../user.interface';
import { UserService } from '../user.service';
/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-user-detalhe',
  templateUrl: './user-detalhe.component.html',
  styleUrls: ['./user-detalhe.component.scss']
})
export class UserDetalheComponent implements OnInit {

  user: User;
  matcher = new MyErrorStateMatcher();
  userForm: FormGroup;
  constructor(
    private userService: UserService,
    private router: ActivatedRoute,
    private fb: FormBuilder,
  ){}

  ngOnInit() {
    this.userService.setUser(this.router.snapshot.params.id);
    this.userService.user.subscribe(
      user => {
        this.user = user;
        this.userForm = this.fb.group({
          saram: [{value: user?.militaryId, disabled:true}],
          name: [{ value: user?.name, disabled:true }],
          email: [{ value: user?.email, disabled:true }],
          cpf: [{ value: user?.cpf, disabled:true }],
          militaryRank: [{ value: user?.militaryRank, disabled:true }],
          militaryName: [{ value: user?.militaryName, disabled:true }],
          militaryCareer: [{ value: user?.militaryCareer, disabled:true }],
          militarySpeciality: [{ value: user?.militarySpeciality, disabled:true }],
          dependente: [{value: user?.dependente, disabled: true}]
        })
      });
  }
}
