import { User } from './../user.interface';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  user: User
  userForm: FormGroup;

  constructor(private userService: UserService, private fb: FormBuilder,) { }

  ngOnInit() {
    this.userService.user.subscribe(
      user => {
        this.user = user;
        this.userForm = this.fb.group({
          saram: [{value: user.militaryId, disabled:true}],
          name: [{ value: user.name, disabled:true }],
          email: [{ value: user.email, disabled:true }],
          cpf: [{ value: user.cpf, disabled:true }],
          militaryRank: [{ value: user.militaryRank, disabled:true }],
          militaryName: [{ value: user.militaryName, disabled:true }],
          militaryCareer: [{ value: user.militaryCareer, disabled:true }],
          militarySpeciality: [{ value: user.militarySpeciality, disabled:true }],
          dependente: [user?.dependente]
        })
      })
  }

  update() {
    this.user.dependente = this.userForm.get('dependente').value;
    this.userService.update(this.user.id, this.user).subscribe(
      user => {
        console.log(user);
      }, err => { console.log('Erro ao atualizar os dados de Usuário.', err); });
  }
}
