import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { UserRouteAccessService } from '../auth/auth';
import { DashboardComponent } from '../layout/dashboard/dashboard.component';


const routes: Routes = [
  {
    path: '', component: PagesComponent, children: [
      { path: '', component: DashboardComponent},
      {
        path: 'users',
        loadChildren: () => import('../pages/user/user.module').then(m => m.UserModule),
        canActivate: [UserRouteAccessService]
      },
      {
        path: 'comissionamentos',
        loadChildren: () => import('../pages/comissionamento/comissionamento.module').then(m => m.ComissionamentoModule),
        canActivate: [UserRouteAccessService]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {
}
