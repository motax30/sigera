import { TestBed } from '@angular/core/testing';

import { ComissionamentoService } from './comissionamento.service';

describe('ComissionamentoService', () => {
  let service: ComissionamentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ComissionamentoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
