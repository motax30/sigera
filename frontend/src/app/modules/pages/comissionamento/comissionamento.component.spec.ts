import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComissionamentoComponent } from './comissionamento.component';

describe('ComissionamentoComponent', () => {
  let component: ComissionamentoComponent;
  let fixture: ComponentFixture<ComissionamentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComissionamentoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComissionamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
