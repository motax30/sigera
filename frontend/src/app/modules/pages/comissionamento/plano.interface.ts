import { Unidade } from './../unidade/unidade.model';
export interface Plano {
  id: number;
  ano: number;
  unidade: Unidade;
  valorTotalPrevisto: number;
  valorTotalExecutado: number;
}
