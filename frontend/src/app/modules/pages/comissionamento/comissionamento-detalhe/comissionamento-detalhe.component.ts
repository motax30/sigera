import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IComissionamento } from '../comissionamento.interface';
import { ComissionamentoService } from '../comissionamento.service';


@Component({
  selector: 'app-comissionamento-detalhe',
  templateUrl: './comissionamento-detalhe.component.html',
  styleUrls: ['./comissionamento-detalhe.component.scss']
})
export class ComissionamentoDetalheComponent implements OnInit {

  comissionamento: IComissionamento;
  form: FormGroup;

  constructor(
    private comissionamentoService: ComissionamentoService,
    private router: ActivatedRoute,
    private fb: FormBuilder,
  ){}

  ngOnInit(): void {
    this.comissionamentoService.setComissionamentoEdit(this.router.snapshot.params.id);
    this.comissionamentoService.comissionamentoEdit.subscribe(
      comissionamento => {
        this.comissionamento = comissionamento;
        this.form = this.fb.group({
          inicio: [{value: comissionamento?.inicio, disabled:true}],
          termino: [{value: comissionamento?.termino, disabled:true}],
          previsaoPromocao: [{value: comissionamento?.previsaoPromocao, disabled:true}],
          diasPlanejados: [{value: comissionamento?.qtdDiasPlanejados, disabled:true}],
          diasExecutados: [{value: comissionamento?.qtdDiasExecutados, disabled:true}],
          mesPagamento: [{value: comissionamento?.mesPagamento, disabled:true}],
          compAno: [{value: comissionamento?.compAno, disabled:true}],
          compAnoSeguinte: [{value: comissionamento?.compAnoSeguinte, disabled:true}],
          valorIdaPlanejado: [{value: comissionamento?.valorIdaPlanejado, disabled:true}],
          valorVoltaPlanejado: [{value: comissionamento?.valorVoltaPlanejado, disabled:true}],
          valorPagoIda: [{value: comissionamento?.valorPagoIda, disabled:true}],
          mesContrachequeIda: [{value: comissionamento?.mesContrachequeIda, disabled:true}],
          valorPagoVolta: [{value: comissionamento?.valorPagoVolta, disabled:true}],
          mesContrachequeVolta: [{value: comissionamento?.mesContrachequeVolta, disabled:true}],
          responsavelCredito: [{value: comissionamento?.responsavelCredito?.nome, disabled:true}],
          ugResponsavel: [{value: comissionamento?.ugResponsavel?.nome, disabled:true}],
          ugPagadora: [{value: comissionamento?.ugPagadora?.nome, disabled:true}],
          nomeUnidade: [{value: comissionamento?.unidade?.nome, disabled:true}],
          name: [{value: comissionamento?.user?.name, disabled:true}],
          plano: [{value: comissionamento?.plano?.id + ' / '+ comissionamento?.unidade?.sigla, disabled:true}]
        })
      });
  }

}
