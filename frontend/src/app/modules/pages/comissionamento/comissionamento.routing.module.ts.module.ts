import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ComissionamentoDetalheComponent } from './comissionamento-detalhe/comissionamento-detalhe.component';
import { ComissionamentoFormComponent } from './comissionamento-form/comissionamento-form.component';
import { ComissionamentoComponent } from './comissionamento.component';
const comissionamentoRoutes = [
  { path: '', component: ComissionamentoComponent },
  { path: ':id', component: ComissionamentoDetalheComponent },
  { path: ':saram/edit', component: ComissionamentoFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(comissionamentoRoutes)],
  exports: [RouterModule]
})
export class ComissionamentoRoutingModule { }
