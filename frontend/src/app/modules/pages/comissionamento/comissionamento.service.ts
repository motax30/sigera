import { ComissionamentoModel } from './comissionamento-model';
import { filter } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { COMISSIONAMENTO_API_URL } from 'src/app/app.constants';
import { EntityService } from '../../shared/services/entity.service';
import { IComissionamento } from './comissionamento.interface';

@Injectable({
  providedIn: 'root'
})
export class ComissionamentoService extends EntityService<IComissionamento> {

  comissionamentos: BehaviorSubject<IComissionamento[]> = new BehaviorSubject(null)
  sub = this.comissionamentos.asObservable()
  comissionamentoEdit: BehaviorSubject<IComissionamento> = new BehaviorSubject(null)

  constructor(protected http: HttpClient) {
    super(http, COMISSIONAMENTO_API_URL);
  }

  setComissionamentos(params=null){
      if (params) {
        this.findMany(params).subscribe(res => this.comissionamentos.next(res))
      } else {
        this.findAll().subscribe( res => this.comissionamentos.next(res))
      }
  }

  setComissionamentoEdit(id: number) {
    this.findOne(id).subscribe(observer =>
      this.comissionamentoEdit.next(observer));
  }
}
