import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { IComissionamento } from './comissionamento.interface';
import { ComissionamentoService } from './comissionamento.service';

@Component({
  selector: 'app-comissionamento',
  templateUrl: './comissionamento.component.html',
  styleUrls: ['./comissionamento.component.scss']
})
export class ComissionamentoComponent implements OnInit {

  comissionamentos: IComissionamento[];
  columnsToDisplay = ['inicio','termino', 'previsaoPromocao', 'qtdDiasPlanejados',
    'qtdDiasExecutados', 'userName',
    'unidadeNome', 'actions'];
  dataSource;

  constructor(private comissionamentoService: ComissionamentoService) { }

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild('filter', { static: true }) filter: ElementRef;

  ngOnInit(): void {
    this.comissionamentoService.setComissionamentos();
    this.comissionamentoService.comissionamentos.subscribe(
      res => {
        this.comissionamentos = res
        this.dataSource = new MatTableDataSource(this.comissionamentos);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
