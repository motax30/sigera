import { Unidade } from './../unidade/unidade.model';
import { User } from './../user/user.model';
export class ComissionamentoModel {
  constructor(
    public url?: string,
    public inicio?: string,
    public termino?: string,
    public previsaoPromocao?: string,
    public qtdDiasPlanejado?: number,
    public qtdDiasExecutado?: number,
    public mesPagamento?: any,
    public compAno?: any,
    public compAnoSeguinte?: any,
    public valorIdaPlanejado?: any,
    public valorVoltaPlanejado?: any,
    public valorPagoIda?: any,
    public mesContrachequeIda?: any,
    public valorPagoVolta?: any,
    public mesContrachequeVolta?: any,
    public responsavelCredito?: string,
    public ugResponsavel?: string,
    public ugPagadora?: any,
    public unidade?: Unidade,
    public user?: User,
    public plano?: string,
  ){}
}
