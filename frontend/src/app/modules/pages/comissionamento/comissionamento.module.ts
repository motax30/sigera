import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AngularMaterialModule } from '../../shared/angular-material.module';
import { ComissionamentoDetalheComponent } from './comissionamento-detalhe/comissionamento-detalhe.component';
import { ComissionamentoFormComponent } from './comissionamento-form/comissionamento-form.component';
import { ComissionamentoComponent } from './comissionamento.component';
import { ComissionamentoRoutingModule } from './comissionamento.routing.module.ts.module';
import { ComissionamentoService } from './comissionamento.service';


@NgModule({
  declarations: [
    ComissionamentoComponent,
    ComissionamentoDetalheComponent,
    ComissionamentoFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ComissionamentoRoutingModule,
    AngularMaterialModule,
    FlexLayoutModule
  ],
  exports: [
    ComissionamentoComponent,
    ComissionamentoDetalheComponent,
    ComissionamentoFormComponent],
  providers: [ComissionamentoService]
})
export class ComissionamentoModule { }
