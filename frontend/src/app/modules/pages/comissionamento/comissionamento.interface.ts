import { Unidade } from './../unidade/unidade.model';
import { User } from './../user/user.interface';
import { Plano } from './plano.interface';
export interface IComissionamento {
  id: number;
  user: User;
  unidade: Unidade;
  plano: Plano;
  inicio: string;
  termino: string;
  previsaoPromocao: string;
  qtdDiasPlanejados: number;
  qtdDiasExecutados: number;
  mesPagamento?: string;
  compAno?: string;
  compAnoSeguinte?: number;
  valorIdaPlanejado?: number;
  valorVoltaPlanejado?: number;
  valorPagoIda?: number;
  mesContrachequeIda?: string;
  valorPagoVolta?: number;
  mesContrachequeVolta?: string;
  responsavelCredito: Unidade;
  ugResponsavel: Unidade;
  ugPagadora?: Unidade;
}
