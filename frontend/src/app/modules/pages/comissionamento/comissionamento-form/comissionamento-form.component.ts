import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IComissionamento } from '../comissionamento.interface';
import { ComissionamentoService } from '../comissionamento.service';


@Component({
  selector: 'app-comissionamento-form',
  templateUrl: './comissionamento-form.component.html',
  styleUrls: ['./comissionamento-form.component.scss']
})
export class ComissionamentoFormComponent implements OnInit {

  comissionamento: IComissionamento;
  form: FormGroup;

  constructor(
    private comissionamentoService: ComissionamentoService,
    private router: ActivatedRoute,
    private fb: FormBuilder,
  ){}

  ngOnInit(): void {
    this.comissionamentoService.setComissionamentoEdit(this.router.snapshot.params.id);
    this.comissionamentoService.comissionamentoEdit.subscribe(
      comissionamento => {
        this.comissionamento = comissionamento;
        this.form = this.fb.group({
          inicio: [comissionamento?.inicio],
          termino: [comissionamento?.termino],
          previsaoPromocao: [comissionamento?.previsaoPromocao],
          diasPlanejados: [comissionamento?.qtdDiasPlanejados],
          diasExecutados: [comissionamento?.qtdDiasExecutados],
          mesPagamento: [comissionamento?.mesPagamento],
          compAno: [comissionamento?.compAno],
          compAnoSeguinte: [comissionamento?.compAnoSeguinte],
          valorIdaPlanejado: [comissionamento?.valorIdaPlanejado],
          valorVoltaPlanejado: [comissionamento?.valorVoltaPlanejado],
          valorPagoIda: [comissionamento?.valorPagoIda],
          mesContrachequeIda: [comissionamento?.mesContrachequeIda],
          valorPagoVolta: [comissionamento?.valorPagoVolta],
          mesContrachequeVolta: [comissionamento?.mesContrachequeVolta],
          responsavelCredito: [comissionamento?.responsavelCredito?.nome],
          ugResponsavel: [comissionamento?.ugResponsavel?.nome],
          ugPagadora: [comissionamento?.ugPagadora?.nome],
          nomeUnidade: [comissionamento?.unidade?.nome],
          name: [comissionamento?.user?.name],
          plano: [comissionamento?.plano?.id + ' / '+ comissionamento?.unidade?.sigla]
        })
      });
  }

}
