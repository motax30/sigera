import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Unidade } from './unidade.model';

@Injectable({
  providedIn: 'root'
})
export class UnidadeService {

  constructor(private http: HttpClient) { }

  getUnidadeDeMilitar(urlUnidade: string) {
    return this.http.get<Unidade>(urlUnidade).pipe(
      map(resp => resp));
  }
}
