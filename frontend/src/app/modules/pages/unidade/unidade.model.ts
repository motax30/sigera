export interface Unidade {
    id: string;
    sigla: string;
    nome: string;
    ativo: boolean;
    grandeComando: string;
    militares: string[];
}
