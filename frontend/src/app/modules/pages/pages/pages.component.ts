import {Component, OnInit} from '@angular/core';

declare var $;

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {
  sideNavOpen = true;
  constructor() {
  }

  ngOnInit() {}

  sideNavToggler() {
    this.sideNavOpen = !this.sideNavOpen;
  }

}
