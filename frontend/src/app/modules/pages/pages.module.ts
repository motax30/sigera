import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages/pages.component';
import { LayoutSigcomModule } from '../layout/layout-sigcom.module';
import { UnidadeComponent } from './unidade/unidade.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { ComissionamentoComponent } from './comissionamento/comissionamento.component';

@NgModule({
  declarations: [
    PagesComponent,
    UnidadeComponent,
  ],
  imports: [
    CommonModule,
    LayoutSigcomModule,
    MatSidenavModule,
    PagesRoutingModule,
  ],
  exports: [
    PagesComponent
  ]
})
export class PagesModule {
}
