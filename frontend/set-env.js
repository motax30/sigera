const fs = require('fs');
const targetPath = './src/environments/environment.ts';

const ANGULAR_IS_PRODUCTION = false;
const DEFAULT_API_BASE_URL = 'http://localhost:8000/';


function getenv(varname) {
    if (varname in process.env) {
        const content = process.env[varname]
        if (!isNaN(parseInt(content)) || content == 'true' || content == 'false') {
            return content
        }
        return '\'' + content + '\'';
    } else {
        return '\'\''; // no lugar de undefined
    }
}

const envConfigFile = `// This file was automatically replaced by script set-env.js
// Last update: ` + JSON.stringify(new Date()) + `

export const environment = {
    production: ` + getenv('ANGULAR_IS_PRODUCTION}') + ` || ` + ANGULAR_IS_PRODUCTION + `,
    apiBaseUrl: ` + getenv('API_BASE_URL') + ` || '` + DEFAULT_API_BASE_URL + `',
};
`;

fs.writeFile(targetPath, envConfigFile, function(err) {
    if (err) {
        return console.log(err);
    }
})